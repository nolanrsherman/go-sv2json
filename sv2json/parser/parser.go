package parser

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"strings"
)

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

/*Parse converts a tsv file into a json formatted byte array using the delimiter specified.*/
func Parse(file io.Reader, delimiter string, root *string, stripKeyWhiteSpace bool) []byte {

	data := parseFile(file, delimiter)
	json := arrToJSON(data, root, stripKeyWhiteSpace)

	return json

}

func arrToJSON(data [][]string, root *string, stripKeyWhiteSpace bool) []byte {
	arr := make([]map[string]string, 0)

	props := make([]string, 0)
	for _, prop := range data[0] {
		props = append(props, prop)
	}

	values := data[1:]

	for _, row := range values {
		obj := make(map[string]string)
		for j := range row {
			key := props[j]
			if stripKeyWhiteSpace {
				key = strings.Replace(key, " ", "", -1)
			}
			obj[key] = row[j]
		}
		arr = append(arr, obj)
	}

	if root != nil {
		root := make(map[string][]map[string]string)
		root["data"] = arr
		js, err := json.MarshalIndent(root, "", "\t")
		if err != nil {
			log.Fatal(err)
		}

		return js

	}

	js, err := json.MarshalIndent(arr, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	return js

}

func parseFile(file io.Reader, delimiter string) [][]string {

	fileScanner := bufio.NewScanner(file)

	data := make([][]string, 0)
	for fileScanner.Scan() {
		line := fileScanner.Text()
		col := strings.Split(line, delimiter)
		data = append(data, col)
	}

	return data

}

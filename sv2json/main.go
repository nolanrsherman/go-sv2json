package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/nolanrsherman/go-sv2json/sv2json/parser"
)

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func main() {
	in := flag.String("i", "", "The file to read from. (default: Stdin")
	out := flag.String("o", "", "The file to write to. (default: Stdout")
	delimiter := flag.String("d", ",", "The file to read from.")
	flag.Parse()

	file := os.Stdin
	if *in != "" {
		f, err := os.Open(*in)
		check(err)
		file = f
	}

	rootName := "root"
	*delimiter = parseDelim(*delimiter)
	json := parser.Parse(*file, *delimiter, &rootName, false)

	//write to stdout or out file
	if *out == "" {
		fmt.Print(string(json))
	} else {
		err := ioutil.WriteFile(*out, json, 0644)
		check(err)
	}

}

func parseDelim(delim string) string {
	switch delim {
	case string([]byte{92, 116}): // "\t"
		return "\t"
	case string([]byte{92, 118}): // "\v"
		return "\v"
	}

	return delim
}

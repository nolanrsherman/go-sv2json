# sv2json

A tool written in go to convert separated value documents into JSON.

sv2json assumes:
- The first row contains the name of the keys for your data
- all rows have the same number of delimiters (comma, tab, etc). 
- each row represents a distinct object.

Sub objects and arrays not supported.

Usage:  
  - -d string  
        The file to read from. (default ",")  
		For  horizontal tab and vertival tab use: `sv2json -d ["\t"]["\v"]` respectively.  
  - -i string  
        The file to read from. (default: Stdin  
  - -o string  
        The file to write to. (default: Stdout  


### Glossary
- [Example](#Example)
- [Installation](#Installation)

# Example

input.csv:
```
col1,col2,col3,col4
a,b,c,d
e,f,g,h
i,j,k,l
```

In terminal run: 
```BASH 
sv2json -d "," -i ./input.csv -o ./output.json
```

output.json
```JSON
{
	"data": [
		{
			"col1": "a",
			"col2": "b",
			"col3": "c",
			"col4": "d"
		},
		{
			"col1": "e",
			"col2": "f",
			"col3": "g",
			"col4": "h"
		},
		{
			"col1": "i",
			"col2": "j",
			"col3": "k",
			"col4": "l"
		}
	]
}
```

# Installation

- Download this project into a directory in your $GOPATH and run from /go-sv2json/sv2json, the `./sv2json` executable will be built to the same directory:
```
go build -o sv2json main.go
```
    
- With go get:
```
go get gitlab.com/nolanrsherman/go-sv2json/sv2json
```